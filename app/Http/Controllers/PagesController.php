<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller{
    
    public function index(){
        return view('Main');

    }
    public function loans(){

        return view('Loans');
   }

     public function consultant(){

        return view('consultant');
   }

   public function investmentmodel(){

    return view('investmentmodel');
   }
   public function contact(){
    return view('contact');
   }
   public function entrepreneur(){
    return view('Entrepreneur');

   }  
   public function investor(){
    return view('Investor');
   } 
 
  public function crowdFunding(){
    return view('CrowdFunding');

  }
  public function earlystageApplication(){

    return view('EarlyStageApplicationform');
  }

  public function expansionstageapplication(){

    return view('ExpansionStageApplicationform');
  }
  public function greenfieldapplication(){

    return view('GreenfieldApplicationform');
  }

  public function investmentcriteria(){

    return view('investmentcriteria');
  }

  public function businessplan(){
    return view('BussinessPlan');
  }

  public function login(){

    return view('auth.login');
  }

  public function register(){

    return view('auth.register');
  }

}
