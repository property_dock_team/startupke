<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loanapplication;
use DB;
class LoanApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
       $postData=new Loanapplication;
       $postData->Applicantname=$request->input('Applicantname');
       $postData->IDNumber=$request->input('IDNumber');
       $postData->Dateofbirth=$request->input('Dateofbirth');
       $postData->Emailaddress=$request->input('Emailaddress');
       $postData->residenttown=$request->input('residenttown');
       $postData->phonenumber=$request->input('phonenumber');
       $postData->maritalStatus=$request->input('maritalStatus');
       $postData->gender=$request->input('gender');
       $postData->IncomeType=$request->input('IncomeType');
       $postData->Employername=$request->input('Employername');
       $postData->businessaddress=$request->input('businessaddress');
       $postData->jobdescription=$request->input('jobdescription');
       $postData->incomerange=$request->input('incomerange');
       $postData->educationlevel=$request->input('educationlevel');
       $postData->seedamount=$request->input('seedamount');
       $postData->projectcost=$request->input('projectcost');
       $postData->repaymentperiod=$request->input('repaymentperiod');
       $postData->purpose=$request->input('purpose');
       $postData->personalContribution=$request->input('personalContribution');
       $postData->currentdebtstatus=$request->input('currentdebtstatus');

       $postData->save();

       return view('Loans.store')->with('success','Application Filled Successfilly');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
