<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactMesssagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_messsagers', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('ApplicantName');
            $table->mediumText('phonenumber');
            $table->longText('EmailAddress');
            $table->mediumText('Message');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_messsagers');
    }
}
