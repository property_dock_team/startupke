<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/loans','PagesController@loans');
Route::get('/investmentmodel', 'PagesController@investmentmodel');
Route::get('/earlystagestartup','PagesController@earlystageApplication');
Route::get('/expansionstagestartup','PagesController@expansionstageapplication');
Route::get('/greenfieldstartup','PagesController@greenfieldapplication');
Route::get('/investmentcriteria', 'PagesController@investmentcriteria');
Route::get('/aboutus','PagesController@businessplan');
Route::get('/contact','PagesController@contact');
Route::get('/entrepreneur','PagesController@entrepreneur');
Route::get('/products','PagesController@investor');
Route::get('/CrowdFunding','PagesController@crowdFunding');
Route::get('/consultant', 'PagesController@consultant');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('Earlystagestartups','EarlyStage');
Route::resource('Expansionstagestartups','ExpansionStage');
Route::resource('GreenFieldstagestartups','GreenFieldStage');
Route::resource('Loans','LoanApplicationController');
Route::resource('contactMessenger','contactMessenger');
