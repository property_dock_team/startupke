@extends('layouts.app')
@section('content')


<section class="mbr-section info2 cid-r7c4jiDTtS" id="info2-1v">

   

    

    <div class="container">
        <div class="row main justify-content-center">
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-4" href="earlystagestartup">Early Stage<br></a></div>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-4" href="expansionstagestartup">Expansion <br></a></div>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-4" href="greenfieldstartup">Green Field<br></a></div>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-4" href="loans"> Loan <br></a></div>
            </div>
            <div class="media-container-column title col-12 col-lg-7 col-md-6">
                <h2 class="align-right mbr-bold mbr-white pb-3 mbr-fonts-style display-2">
                    THANK YOU FOR APPLYING.WE WILL CONTACT YOU IF YOU QUALIFY</h2>
                
            </div>
        </div>
    </div>
</section>

@endsection