<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/img-20180813-wa0009-122x122.jpg" type="image/x-icon">
  <meta name="description" content="Web Page Builder Description">
  <title>Contact</title>
  <!-- <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css"> -->

  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WJwW81bG" once="menu" id="menu2-10">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="#">how to build a website</a></section><section class="mbr-section form4 cid-r6WEV4ViTv" id="form4-v">

    

    
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="google-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.899190641248!2d36.83923694989403!3d-1.2298660359086449!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f163ecc0551a3%3A0xbcf8633bde1b3af3!2sNEWRIS%20AUTOS!5e0!3m2!1sen!2ske!4v1633688490666!5m2!1sen!2ske" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
            </div>
            <div class="col-md-6">
                <h2 class="pb-3 align-left mbr-fonts-style display-2">
                    Drop a Line
                </h2>
                <div>
                    <div class="icon-block pb-3">
                        <span class="icon-block__icon">
                            <span class="mbri-letter mbr-iconfont"></span>
                        </span>
                        <h4 class="icon-block__title align-left mbr-fonts-style display-5">
                            Do not hesitate to contact us
                        </h4>
                    </div>
                    <div class="icon-contacts pb-3">
                        <h5 class="align-left mbr-fonts-style display-7">
                            Ready for offers and cooperation
                        </h5>
                        <p class="mbr-text align-left mbr-fonts-style display-7">
                            Phone: 254715576211<br>Tel: &nbsp; &nbsp;+254729379752<br>
                            Email: info@startup.co.ke 
                        </p>
                    </div>
                </div>
                {!! Form::open(['class' => 'block mbr-form','action' => 'contactMessenger@store', 'method'=>'POST'])!!}
                <div data-form-type="formoid">
                    <div data-form-alert="" hidden="">
                        Thanks for filling out the form!
                    </div>
                    
                    {{-- <form class="block mbr-form" action="#" method="post" data-form-title="Mobirise Form"> --}}
                        <input type="hidden" name="email" data-form-email="true" value="tVzs2tG9X6J4rVgp3JS6JakTCJ609DeunDmlFy7A76pc6iXTxTNkqDNIRSBpN5Z/QYyUNupWy12Yiz1wJYHxxPbJU+7dID/qGLcjTU0H7W4T8Wo/CCfAi7GNsg5w1YPV">
                        <div class="row">
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="ApplicantName" data-form-field="Name" placeholder="Your Name" required="" id="name-form4-v">
                            </div>
                            <div class="col-md-6 multi-horizontal" data-for="phone">
                                <input type="text" class="form-control input" name="phonenumber" data-form-field="Phone" placeholder="Phone" required="" id="phone-form4-v">
                            </div>
                            <div class="col-md-12" data-for="email">
                                <input type="text" class="form-control input" name="EmailAddress" data-form-field="Email" placeholder="Email" required="" id="email-form4-v">
                            </div>
                            <div class="col-md-12" data-for="message">
                                <textarea class="form-control input" name="Message" rows="3" data-form-field="Message" placeholder="Message" style="resize:none" id="message-form4-v"></textarea>
                            </div>
                            <div class="input-group-btn col-md-12" style="margin-top: 10px;">
                                <button href="" type="submit" class="btn btn-primary btn-form display-4">SEND MESSAGE</button>
                            </div>
                        </div>
                        {{-- </form> --}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>

<section class="cid-r6WFoQnqey" id="social-buttons3-x">
    
    

    

    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-section-title mbr-fonts-style display-2">
                    SHARE THIS PAGE!
                </h2>
                <div>
                    <div class="mbr-social-likes">
                            <a href="https://www.facebook.com/StartupKenya254/" target="_blank">
                                <span class="btn btn-social socicon-bg-facebook facebook mx-2" title="Share link on Facebook">
                                        <i class="socicon socicon-facebook"></i>
                                    </span>
                            </a>
                     
                        <a href="https://twitter.com/Startupkenya254" target="_blank">
                            <span class="btn btn-social twitter socicon-bg-twitter mx-2" title="Share link on Twitter">
                                    <i class="socicon socicon-twitter"></i>
                                </span>
                        </a>
                       
                        <a href="https://www.instagram.com/StartupKenya254/" target="_blank">
                            <span class="btn btn-social plusone socicon-bg-instagram mx-2" title="Share link on Instagram">
                                    <i class="socicon socicon-instagram"></i>
                                </span>
                        </a>
                      
                        <a href="https://www.linkedin.com/company/startup-kenya" target="_blank">
                            <span class="btn btn-social vkontakte socicon-bg-linkedin mx-2" title="Share link on Linked In">
                                    <i class="socicon socicon-linkedin"></i>
                                </span>
                        </a>
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
<script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>

  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/sociallikes/social-likes.js')}}"></script>
  <script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  <script src="{{ asset('css/assets/formoid/formoid.min.js')}}"></script>
  
  
</body>
@endsection