@extends('layouts.appMain')
@section('content')
<section class="mbr-section info2 cid-r7c4jiDTtS"><a href="">free website creation software</a></section><section class="mbr-section info2 cid-r7c4jiDTtS" id="header12-6">

<div class="row " style="margin-left: 70px">
<div class="col-md-5 col-md-offset-1">

{!! Form::open(['action'=>'LoanApplicationController@store', 'method'=>'POST']) !!}

  <div class="form-group">
  	<h3> PERSONAL INFORMATION</h3>
    <label for="exampleInputEmail1">Applicant Name (Mr/Mrs/Miss/Dr/Prof)</label>
    <input type="text" class="form-control" required=""  name="Applicantname" placeholder="">
  </div>

 <div class="form-group">
  	
    <label for="exampleInputEmail1">National ID/Passport No.</label>
    <input type="text" required="" class="form-control" name="IDNumber" placeholder="">
  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Date Of Birth</label>
    <input type="date" required="" class="form-control" name="Dateofbirth" placeholder="">

  </div>

   <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Email Address</label>
    <input type="email" required="" class="form-control" name="Emailaddress" placeholder="">

  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Resident Town</label>
    <input type="text" required="" class="form-control" name="residenttown" placeholder="">

  </div>

   <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Telephone No</label>
    <input type="text" required=""  class="form-control" name="phonenumber" placeholder="">

  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Marital status </label>
    <select class="form-control" name="maritalStatus">
      <option>Maried</option>
      <option>Single</option>
      <option>Devorced</option>
    </select>
  </div>


 <div class="form-group">
    <label for="exampleInputEmail1">Gender</label>
    <select class="form-control" name="gender">
      <option>Male</option>
      <option>Female</option>
      
    </select>
  </div>

 



</div>



<div class="col-md-5 col-md-offset-1">
	

	<h3> INCOME SOURCE</h3>
{{--  <label for="exampleInputEmail1">Income Type</label>
<div class="input-group">

   <input type="text" class="form-control" aria-label="...">
  <div class="input-group-btn">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Income type <span class="caret"></span></button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu">
      <li><a href="#">Employment</a></li>
      <li><a href="#">Self Employed</a></li>
      <li><a href="#">Pension</a></li>
      <li class="divider"></li>
      <li><a href="#">Business Owner</a></li>
    </ul>
  </div><!-- /btn-group -->
</div><!-- /input-group -->  --}}

<div class="form-group">
  <label for="sel1">Income Type:</label>
  <select class="form-control" name="IncomeType">
    <option>Employment</option>
    <option>Self Employed</option>
    <option>Pension</option>
    <option>Business Owner</option>
  </select>
</div>

  <div class="form-group">
      <label for="exampleInputEmail1">Name of Employer / Name of your Business</label>
    <input type="text" required="" class="form-control" name="Employername" placeholder="">
  </div>

 <div class="form-group">
  	
    <label for="exampleInputEmail1">Physical address: where do you work</label>
    <input type="text" required="" class="form-control" name="businessaddress" placeholder="">
  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">What is your Job description</label>
    <input type="text" required="" class="form-control" name="jobdescription" placeholder="">

  </div>

   <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Income Range</label>
    <select class="form-control" name="incomerange">
      <option>10K  - 40k</option>
      <option>40K  - 80k</option>
      <option>80K  - 120k</option>
      <option>120K - 150k</option>
      
    </select>

  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">What is Your Education Level</label>
    <select class="form-control" name="educationlevel">
      <option>Primary School</option>
      <option>High School</option>
      <option>Certificate</option>
      <option>Diploma</option>
      <option>Undergraduate</option>
      <option>Graduate</option>
      <option>Post Graduate</option>
      
    </select>

  </div>

   {{--  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Telephone No</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">

  </div>  --}}





</div>
</div>

<div class="row" style="margin-left: 70px">
<div class="col-md-5 col-md-offset-1">


  <div class="form-group">
  	<h3>LOAN PARTICULARS</h3>
    <label for="exampleInputEmail1">Amount applied for (Kshs)</label>
    <input type="text" required=""  class="form-control" name="seedamount" placeholder="">
  </div>

 <div class="form-group">
  	
    <label for="exampleInputEmail1">Cost of project (Kshs)</label>
    <input type="text" required="" class="form-control" name="projectcost" placeholder="">
  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Repayment period in Months</label>
    <input type="text" required="" class="form-control" name="repaymentperiod" placeholder="">

  </div>

   <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Purpose</label>
    <textarea class="form-control" required=""  rows="3" name="purpose"></textarea>

  </div>

  <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Personal contribution (Kshs)</label>
    <input type="text" required=""  class="form-control" name="personalContribution" placeholder="">

  </div>

   <div class="form-group">
  	
 
    <label for="exampleInputEmail1">Do you currently have any Debt or Liability </label>
      <select class="form-control" name="currentdebtstatus">
        <option>Yes</option>
        <option>No</option>
        
      </select>

  </div>





  <button type="submit" class="btn btn-default">Submit</button>
  {!! Form::close() !!}
</div>




</div>
</div>
</section>	
@endsection