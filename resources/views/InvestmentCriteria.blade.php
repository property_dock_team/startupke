
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{  asset('css/assets/images/img-20180813-wa0009-122x122.jpg')}}" type="image/x-icon">
  <meta name="description" content="Web Page Creator Description">
  <title>Investment Critaria</title>
  {{--  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">  --}}
  
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
</head>
<body>
  {{--  <section class="menu cid-r6WJbhG2ku" once="menu" id="menu2-1h">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="">web templates free download</a></section><section class="mbr-section content5 cid-r70dl0S0zw mbr-parallax-background" id="content5-1i">

    

    

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    <br>Investment Criteria</h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">We seek profitable businesses with the following features:</h3>
                
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content12 cid-r70dHKOdqX" id="content12-1j">
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text counter-container col-12 col-md-8 mbr-fonts-style display-7">
                <ul>
                    <li><strong>PROFITABILITY</strong> - The ability to generate strong, sustainable cash flows and deliver an Internal Rate of Return of at least 30%.</li>
                    <li><strong>INVESTMENT SIZE</strong> - An investment requirement of between Ksh.50,000 and Ksh. 1.5M. This may be structured as an equity investment, or as a participating/convertible loan. The entrepreneur must have evidence of what he or she will contribute to the venture in cash or assets.</li>
                    <li><strong>MANAGEMENT</strong> - A management team, with a relevant track record, exists or can be mobilized (as we cannot provide management expertise). This team should be open to working with active and involved hands-on investors.</li><li><strong>PRICE OFFERING</strong> - An initial offering price is in place. This will help us quickly decide whether the opportunity is attractive.</li><li><strong>INVESTMENT MATURITY</strong> - Clear opportunities for investor to exit .</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content11 cid-r70eRl63kM" id="content11-1k">
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text counter-container col-12 col-md-8 mbr-fonts-style display-7">
                <ol>
                    <li><strong>Geographic Focus</strong><span style="font-size: 1rem;">&nbsp;- Startup ventures will invest only in companies incorporated in, or enterprises considering expansion into, Kenya.</span><br></li>
                    <li><strong>Company Stage</strong>&nbsp;- Startup ventures will invest in startups and expansion-stage companies.</li>
                    <li><strong>Industry Focus</strong>&nbsp;- Startup ventures has a broad industry focus. We generally prefer agribusinesses, education, healthcare, and general industry,Technology, Entertainment, Transport and general industry</li><li><strong>Investment Size </strong>-<strong> </strong>Startup ventures will invest Ksh.50,000 to Ksh. 1.5M in each portfolio company. The investment company will generally invest in two or more rounds based on the accomplishment of milestones by the portfolio company.</li><li><strong>Type of Security</strong> - Startup ventures generally will make equity investments in preferred stock and loan investments in convertible or participating loans.</li><li><strong>Investment Term</strong> &nbsp;-The anticipated time for holding a typical investment is three to five years.</li><li><strong>Investment Term</strong> &nbsp;- &nbsp;The anticipated time for holding a typical investment is three to five years.</li><li><strong>Likely Exit Scenarios</strong> &nbsp;- The most likely exit scenarios are trade sales or buyouts by the entrepreneur. However, other exit scenarios are possible.</li><li><strong>Co-investors</strong> - &nbsp; Startup ventures will be a sole investor in many cases given that it takes higher risk than other investors are typically able to. Occasionally, in cases where the required investment exceeds our maximum investment size, Startup may invest as part of an investment syndicate or co-invest in a round led by another institutional investor. When appropriate, Startup ventures will co-invest with other financial institutions.</li></ol><ol>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content10 cid-r70gJzmxf8" id="content10-1l">
    
     

    <div class="container">
        <div class="inner-container" style="width: 66%;">
            <hr class="line" style="width: 25%;">
            <div class="section-text align-center mbr-white mbr-fonts-style display-5">Entrepreneurs with businesses or business plans that meet the above criteria in Kenya are invited to contact us or to submit their invitation to invest to us for our consideration.</div>
            <hr class="line" style="width: 25%;">
        </div>
    </div>
</section>

<section class="cid-r70hdvb7am" id="social-buttons3-1n">
    
    

    

    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-section-title mbr-fonts-style display-2">
                    SHARE THIS PAGE!
                </h2>
                <div>
                    <div class="mbr-social-likes">
                        <span class="btn btn-social socicon-bg-facebook facebook mx-2" title="Share link on Facebook">
                            <i class="socicon socicon-facebook"></i>
                        </span>
                        <span class="btn btn-social twitter socicon-bg-twitter mx-2" title="Share link on Twitter">
                            <i class="socicon socicon-twitter"></i>
                        </span>
                        <span class="btn btn-social plusone socicon-bg-googleplus mx-2" title="Share link on Google+">
                            <i class="socicon socicon-googleplus"></i>
                        </span>
                        <span class="btn btn-social vkontakte socicon-bg-vkontakte mx-2" title="Share link on VKontakte">
                            <i class="socicon socicon-vkontakte"></i>
                        </span>
                        <span class="btn btn-social odnoklassniki socicon-bg-odnoklassniki mx-2" title="Share link on Odnoklassniki">
                            <i class="socicon socicon-odnoklassniki"></i>
                        </span>
                        <span class="btn btn-social pinterest socicon-bg-pinterest mx-2" title="Share link on Pinterest">
                            <i class="socicon socicon-pinterest"></i>
                        </span>
                        <span class="btn btn-social mailru socicon-bg-mail mx-2" title="Share link on Mailru">
                            <i class="socicon socicon-mail"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
<script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
<script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
<script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
<script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
<script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
<script src="{{ asset('css/assets/vimeoplayer/jquery.mb.vimeo_player.js')}}"></script>
<script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
{{--  
  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/parallax/jarallax.min.js"></script>
  <script src="assets/sociallikes/social-likes.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
    --}}
  
</body>
@endsection