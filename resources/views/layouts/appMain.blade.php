<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Start Up') }}</title>

    <!-- Styles -->
    {{--  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--  <link href="{{ asset('css/creative.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/4.10.1/bootstrap-social.css" rel="stylesheet" >  --}}  --}}


     {{--  <link href="{{ asset('css/creative.css') }}" rel="stylesheet">
    <script src="{{ asset('js/creative.js')}}"></script>    --}}
   
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{  asset('css/assets/images/img-20180813-wa0009-122x122.jpg')}}" type="image/x-icon">
  <meta name="description" content="">
  <title>Home</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  

</head>
<body>
    <div id="app">
            <section class="menu cid-r6WJNeA9ds" once="menu" id="menu2-11">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="menu-logo">
                        <div class="navbar-brand">
                            <span class="navbar-logo">
                                <a href="{{ url('/') }}">
                                    <img src="{{asset('css/assets/images/logo2.png')}}" alt="Mobirise" style="height: 3.8rem;">
                                </a>
                            </span>
                            
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                                <a class="nav-link link text-black display-4" href="/">
                                    Home</a>
                            </li><li class="nav-item {{ Request::is('Investor') ? 'active' : '' }}"><a class="nav-link link text-black display-4" href="Investor">
                                    Services</a></li><li class="nav-item {{ Request::is('Entrepreneur') ? 'active' : '' }}"><a class="nav-link link text-black display-4" href="Entrepreneur">
                                    Our Product</a></li>
                            <li class="nav-item {{ Request::is('CrowdFunding') ? 'active' : '' }}">
                                <a class="nav-link link text-black display-4" href="CrowdFunding">
                                    Crowd Funding</a>
                            </li>
                                    <li class="nav-item {{ Request::is('businessplan') ? 'active' : '' }}"><a class="nav-link link text-black display-4" href="businessplan">
                                            Business Plan</a></li>
                                            <li class="nav-item {{ Request::is('investmentcriteria') ? 'active' : '' }}"><a class="nav-link link text-black display-4" href="investmentcriteria">
                                                    Investment Type </a></li>
                                                    <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a class="nav-link link text-black display-4" href="contact">
                                                            Contacts</a></li>
                                
                                
                                </ul>
                                <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                                        <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                                        </span>
                                        +254 715576211</a></div>
                            </div>
                                {{--  <ul class="nav navbar-nav navbar-right">
                                        <!-- Authentication Links -->
                                        @if (Auth::guest())
                                            <li class="nav-item {{ Request::is('login') ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                                            <li class="nav-item {{ Request::is('register') ? 'active' : '' }}"><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                            <li class="nav-item dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </a>
                
                                                <ul class="dropdown-menu" role="menu">
                                                    <li class="nav-item "
                                                        <a href="{{ route('logout') }}"
                                                            onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>
                
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endif
                                    </ul>  --}}
                    </div>
                    
                </nav>
            </section>
        {{--  <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Start Up') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                <li class="{{ Request::is('/') ? 'active' : '' }}">
                            <a href="/">Home</a>
                        </li>
                        <li class="{{ Request::is('Investor') ? 'active' : '' }}">
                            <a href="Investor">Investors<br></a>
                        </li>
                        <li class="{{ Request::is('Entrepreneur') ? 'active' : '' }}">
                            <a href="Entrepreneur">Enterpreneur<br></a>
                        </li>
                        <li class="{{ Request::is('CrowdFunding') ? 'active' : '' }}">
                            <a href="CrowdFunding">Crowd Funding</a>
                        </li>
                      
                        <li class="{{ Request::is('loans') ? 'active' : '' }}">
                            <a href="consultant">Consultants</a>
                        </li>
                      
                        <li class="{{ Request::is('contact') ? 'active' : '' }}">
                            <a href="contact">Contact</a>
                        </li>
                       
                        </ul>
            </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li class="{{ Request::is('login') ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                            <li class="{{ Request::is('register') ? 'active' : '' }}"><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>  --}}
        @include('inc.messages')
       @yield('content')
       
       
     </div>
     
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

   
</body>


</html>
