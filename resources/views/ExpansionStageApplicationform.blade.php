@extends('layouts.appMain')
@section('content')
{{--  <link href="{{ asset('css/creative.css') }}" rel="stylesheet">
<script src="{{ asset('js/creative.js')}}"></script>  --}}
<section class="mbr-section info2 cid-r7c4jiDTtS"><a href="">free website creation software</a></section>
<section class="mbr-section info2 cid-r7c4jiDTtS" id="header12-6">

<div class="row" style="margin-left: 20px">
    <div class="col-md-5 ">
            {!! Form::open(['action'=>'ExpansionStage@store', 'method'=>'POST','enctype'=>'multipart/form-data'])!!}
                <div class="form-group">
                    <h3>Company Information</h3>
                    <label for="exampleInputEmail1"> Please enter the name of your company:</label>
                    <input type="text" required="" class="form-control input" name="companyName" placeholder="">
                </div>
                <div class="form-group">
                  
                  <label for="exampleInputEmail1">Telephone Contact:</label>
                  <input type="text" required="" class="form-control input" name="phoneContact" placeholder="">
              </div>
              <div class="form-group">
                  
                <label for="exampleInputEmail1">Email Address:</label>
                <input type="email" required="" class="form-control input" name="EmailAddress" placeholder="">
            </div>
            <div class="form-group">
                  
              <label for="exampleInputEmail1">CEO of your company:</label>
              <input type="text" required="" class="form-control input" name="CEOName" placeholder="">
          </div>

          <div class="form-group">
                  
            <label for="exampleInputEmail1">Location of your company:</label>
            <input type="text" required="" class="form-control input" name="location" placeholder="">
        </div>
          <div class="form-group">
               
            <label for="exampleInputEmail1">Business Type </label>
            <select class="form-control" name="businessType">
                <option>Sole Proprietorship </option>
                <option>Limited Partnership</option>
                <option>Limited Liability Company</option>
                <option>Nonprofit Organization</option>
                
              </select>
                           
        </div>

        <div class="form-group">
               
          <label for="exampleInputEmail1">KRA PIN</label>
          <input type="text" required="" class="form-control input" name="KRAPIN" placeholder="">
                         
      </div>
                             
     </div>
     <div class="col-md-6 ">
       
            
        <div class="form-group">

                <h3>Product or Service Offered</h3>
            <label for="sel1">Industry:</label>
            <select class="form-control" name="ProductOrService">
              <option>Software Tech</option>
              <option>Agri-Business</option>
              <option>Education</option>
              <option>Entertainment</option>

              <option>Bars & Restaurants</option>
              <option>Designer & Fashion Ware</option>
              <option>Building & Construction</option>
              <option>Environment</option>
              <option>Health Care clinics</option>
              <option>Liquor, Wine & Beer</option>
              <option>Hotel & Catering</option>
              <option>Music Production</option>
              <option>Pharmaceuticals & Health Products</option>
              <option>Poultry & Eggs</option>
              <option>Printing & Publishing</option>
              <option>Real Estate</option>

              <option>Retail Sales</option>
              <option>Private Sequrity</option>
              <option>Transport</option>
              <option>Sports</option>
              <option>Textiles </option>
              <option>Trucking & Logistics</option>
              <option>Vegetables & Fruits Farming</option>
              <option>Waste Management</option>
              <option>Dairy Farming</option>
              <option>Poultry & Eggs Farming</option>
              <option>Travel $ Tourism</option>
              <option>Social Media Marketing</option>
            </select>
          </div>

          <div class="form-group">
               
            <label for="exampleInputEmail1">what Does Your Business Offer </label>
            <select class="form-control" name="CommodityType">
                <option>Products Goods </option>
                <option>Services</option>
              
              </select>
                           
        </div>

            <div class="form-group">
                
                <label for="exampleInputEmail1">Description of Product or Service Offered</label>
                <textarea class="form-control" required="" rows="5" name="Description"></textarea>
                <label for="exampleInputEmail1">What is your Competitive Advantage</label>
                <select class="form-control" name="CompetitiveAdvanage">
                    <option>Pattent / Trademark / Copyright</option>
                    <option>Unique Formula / new Innovation</option>
                    <option>Strategic Location</option>
                    <option>Strategic connection or network</option>
                    <option>Talent / Experience</option>

                  </select>           
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"> where are You in the Supply Chain </label>
                <select class="form-control" name="supplyChain">
                    <option>Manufacturer / Service Provider</option>
                    <option>Whole Seller Distributer</option>
                    <option>Broker / Intermediary</option>
                    <option>Retailler</option>
                  </select>
                          
            </div>
            <div class="form-group">
               
                <label for="exampleInputEmail1"> Product Re-Usability</label>
                <select class="form-control" name="reusability">
                    <option>One Time Usage </option>
                    <option>Re-usable Product</option>
                  
                  </select>
                               
            </div>
            

 </div>
 <div class="col-md-5 ">
    
     
    <div class="form-group">
        <h3>FINANCIAL MANAGEMENT </h3>
      <label for="exampleInputEmail1">Amount applied for (Kshs)</label>
      <input type="number" required="" class="form-control" name="amount" placeholder="">
    </div>

    <div class="form-group">
            <label for="exampleInputEmail1">Investment Instrument</label>
            <select class="form-control" name="investmentInstrument">
               <option>Equity share</option>
               <option>Convertible Loan</option>
               <option>Initial Startup Offering (ISO)</option>
             </select>
         
           </div>
  
   <div class="form-group">
        
      <label for="exampleInputEmail1">Cost of project (Kshs)</label>
      <input type="number" required="" class="form-control" name="projectCost" placeholder="">
    </div>
  
    
  
     <div class="form-group">
        
   
      <label for="exampleInputEmail1">Purpose</label>
      <textarea class="form-control" required="" rows="3" name="Purpose"></textarea>
  
    </div>
  
    <div class="form-group">
        
   
      <label for="exampleInputEmail1">Personal contribution (Kshs)</label>
      <input type="number" required="" class="form-control" name="personalContribution" placeholder="">
  
    </div>
  

    <div class="form-group">
            <label for="exampleInputEmail1">Do you have any Existing Liability or Debt</label>
            <select class="form-control" name="Liability">
               <option>Yes</option>
               <option>No</option>
           
             </select>
         
           </div>
    
           
    <div class="form-group">
            <label for="exampleInputEmail1">What is your Main Business Cost</label>
            <select class="form-control" name="mainbusinessCost">
               <option>Production Cost</option>
               <option>Marketing cost</option>
               <option>Transportation cost</option>
               <option>Rent cost</option>
               <option>Salaries & Wages cost</option>

             </select>
         
           </div>
        
        <div class="checkbox">
        </div>
             
        </div>


        <div class="col-md-6 ">
          


            <div class="form-group">
                <h3> SALES AND MARKETING</h3>
              <label for="exampleInputEmail1">Target Market Group</label>
              <select class="form-control" name="targetmarket">
                <option>Cooprates Clients</option>
                <option>Retail Clients</option>
                
              </select>
            </div>
            <div class="form-group">
                
                <label for="exampleInputEmail1">Income Group Of Client</label>
                <select class="form-control" name="incomeGroup">
                    <option>Low and Average Income customers</option>
                    <option>High Networth & income customers </option>
                    
                  </select>
              </div>
          
           <div class="form-group">
                
              <label for="exampleInputEmail1">Who are the Industry giants in your industry</label>
              <textarea class="form-control" required=""  rows="2" name="industrygiants"></textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Product Delivery Mode</label>
                <select class="form-control" name="deliveryMode">
                   <option>Retail shop Location</option>
                   <option>Physical delivery</option>
                   <option>Parcel Delivery</option>
                 </select>
             
               </div>
          
            <div class="form-group">
                
           
              <label for="exampleInputEmail1">What is your main mode of Marketing</label>
                  <select class="form-control" name="marketingMode">
                  <option>Digital Marketing : Radio / TV</option>
                  <option>Social Media Marketing</option>
                  <option>Evangelism / Word of mouth Marketing</option>
                  <option>Print media Marketing</option>
                  <option>Billboard Marketing</option>
                </select>
          
            </div>
          
                <div class="form-group">
                <label for="exampleInputEmail1">Product Resellability</label>
                <select class="form-control" name="productResellability">
                    <option>Re-usable Product / Service : One time Sale</option>
                    <option>One time usage Product / Service : Repeat sale</option>
                
                </select>
            
                </div>
          
            <div class="form-group">
                <label for="exampleInputEmail1">How do you intend to grow Revenue</label>
                <select class="form-control" name="businessScalability">
                   <option>Increase volume of sales</option>
                   <option>High Magrins on each sale</option>
               
                 </select>
             
               </div>
        </div>
        <div class="form-group">
            <h3>Additional Documents</h3>
            <label for="exampleInputFile">Current Business Plan </label>
            <br>
            <input type="file" name="fileBusinessPlan">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
        <div class="checkbox">
        </div>
           
          
           
        </div>
 {!! Form::close() !!}
</div>
</div>


</section>
@endsection































