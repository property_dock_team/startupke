@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-1">
    <form class="">
        <div class="form-group">
            <h3>Company Name</h3>
            <label for="exampleInputEmail1"> Please enter the name of your company:</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
        </div>
        <div class="form-group">
            <h3>Management Team Information</h3>
            <label for="exampleInputEmail1">Founders, other C- level Executives and Employees by function.</label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">What is the length of time the management team has worked together? And in what capacity?</label>
            <textarea class="form-control" rows="2"></textarea>             
            <label for="exampleInputEmail1">List of Board of Directors and Advisors</label>
            <textarea class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <h3>Company Summary </h3>
            <label for="exampleInputEmail1"> Please enter your short pitch here:</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
        </div>
        <div class="form-group">
            <h3>Product and Core Technology</h3>
            <label for="exampleInputEmail1">Description of Product and Core Technology.</label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">What is the status of your Intellectual Property</label>
            <textarea class="form-control" rows="2"></textarea>             
        </div>
        <div class="form-group">
            <h3>Target Market:</h3>
            <label for="exampleInputEmail1"> Description and estimated size of total market </label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">Description and estimated size of your addressable market </label>
            <textarea class="form-control" rows="2"></textarea>             
        </div>
        <div class="form-group">
            <h3>Customers:</h3>
            <label for="exampleInputEmail1"> Do you have current customers or letter of intent? Please identify customers. </label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">What is the current customer pain and need to cure it? </label>
            <textarea class="form-control" rows="2"></textarea>             
            <label for="exampleInputEmail1">Definition of your target customer </label>
            <textarea class="form-control" rows="2"></textarea>             
            <label>
                <label for="exampleInputEmail1">What is your Current estimated annual revenue? </label>
                <input type="checkbox" value="">
                below  ksh.500k
            </label>             
            <label>
                <input type="checkbox" value="">
                ksh.500k - 1million
            </label>             
            <label>
                <input type="checkbox" value="">
                over 1million
            </label>             
        </div>
        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile">
        </div>
        <div class="checkbox">
</div>
        <button type="submit" class="btn btn-default">Submit</button>
</div>
@endsection






























