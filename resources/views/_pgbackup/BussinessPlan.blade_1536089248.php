<html lang="en">
    <head>
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Custom styles for this template
      
    -->
        <link href="justified-nav.css" rel="stylesheet">
        <title> startup Ventures </title>
    </head>
    <body class="">
        <div class="container">
            <div class="masthead">
                <h3 class="text-muted">Start up</h3>
                <nav class="navbar navbar-inverse">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="Investor">Investors<br></a>
                        </li>
                        <li>
                            <a href="Entrepreneur">Enterpreneur<br></a>
                        </li>
                        <li>
                            <a href="CrowdFunding">Crowd Funding</a>
                        </li>
                        <li>
                            <a href="loans">Loans and Credit</a>
                        </li>
                        <li>
                            <a href="contact">Contact</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="login">Log in</a>
                        </li>
                        <li>
                            <a href="register">Sign up</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div id="wrap" class="">
                <div id="header" class="fixed">
                    <!-- ///   CONTENT   /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                    <div class="fixed">
                        <div class="col580">
                            <!-- ///  MAIN CONTENT  //////////////////////////////////////////////////////////////////////////////////////////////// -->
                            <h1 class="">Business Plans</h1>
                            <p class="justified">For us to consider any invitation to invest, we need something that spells out clearly the business opportunity, who is going to deliver on the opportunity, and how we are invited to participate (including the expected returns from the venture).</p> 
                            <blockquote>We prefer business plans that are clear and simple. Every company should develop and use a succinct written business plan to guide its operations.</blockquote>
                            <p class="justified">We expect a company's business plan to convey clearly the potential of the investment opportunity. We do not expect a business plan to address every conceivable question. What's important to us is the quality of an idea, the nature of the opportunity, and the commitment management makes to achieving ambitious and attainable goals. </p>
                            <p class="justified">While business plans come in many varieties, and there are a number of acceptable ways to structure and present a business plan, we look for several key elements when we evaluate a business plan.</p> 
                            <div class="hr"></div>
                            <h3>Business Plan Outline</h3>
                            <p class="justified">Minimum information required to submit for an investment appraisal from Kinyeti Venture Capital:</p> 
                            <ul class="checklist"> 
                                <li>
                                    <strong>Executive Summary</strong>
                                </li>
                                <ul>
                                    <li>What are your expectations of Kinyeti as an investor?</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Company Description</strong>
                                </li>
                                <ul>
                                    <li>How it started?</li>
                                    <li>Where you are and where do you want to go?</li>
                                    <li>Who are the owners?</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Product/ Service</strong>
                                </li>
                                <ul>
                                    <li>What are you selling?</li>
                                    <li>How does your product differentiate itself?</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Market Analysis</strong>
                                </li>
                                <ul>
                                    <li>What is going on in the industry?</li>
                                    <li>Who are your customers and competitors?</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Marketing Plan</strong>
                                </li>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Operations Plan</strong>
                                </li>
                                <ul>
                                    <li>How are operations executed?</li>
                                    <li>What are the key variables?</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Financial Plan</strong>
                                </li>
                                <ul>
                                    <li>Historical &amp; Projected Financials</li>
                                    <li>P&amp;L, BS, Cash Flow statements</li>
                                    <li>Financing Requirements</li>
                                </ul>
                            </ul>
                            <ul class="checklist"> 
                                <li>
                                    <strong>Management Team</strong>
                                </li>
                                <ul>
                                    <li>Who is the core management team?</li>
                                    <li>What is their experience and incentives?</li>
                                </ul>
                            </ul>
                            <div class="hr"></div>
                            <h3>Are you a Candidate?</h3>
                            <p class="justified">If you believe that your business meets the requirements we have outlined, </p>
                            <button>APPLY</button>                             
                            <p class="justified">Alternatively, you can send us your business plan and investment proposal by <a href="mailto:info@kinyeticapital.com">email</a> (as indicated on the <a href="http://kinyeticapital.com/index.php/pages/contact">"Contact Us"</a> page) or by using the <a href="http://kinyeticapital.com/index.php/pages/contact">upload tool</a> on the <a href="http://kinyeticapital.com/index.php/pages/contact">Contact Us</a> page.</p> 
                            <p class="justified">We will respond to you upon receiving communication from you.</p> 
    </body>
</html>
