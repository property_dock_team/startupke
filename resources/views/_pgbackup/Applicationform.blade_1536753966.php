@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-1">
    <form>
        <div class="form-group">
            <h2>Company Name</h2>
            <label for="exampleInputEmail1"> Please enter the name of your company:</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
            <h2>Management Team Information</h2>
            <label for="exampleInputEmail1">Founders, other C- level Executives and Employees by function.</label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">What is the length of time the management team has worked together? And in what capacity?</label>
            <textarea class="form-control" rows="2"></textarea>             
            <label for="exampleInputEmail1">List of Board of Directors and Advisors</label>
            <textarea class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <h2>Company Summary (“One Line Pitch”)</h2>
            <label for="exampleInputEmail1"> Please enter your short pitch here:</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
            <h2>Product and Core Technology</h2>
            <label for="exampleInputEmail1">Description of Product and Core Technology.</label>
            <textarea class="form-control" rows="3"></textarea>
            <label for="exampleInputEmail1">What is the status of your Intellectual Property</label>
            <textarea class="form-control" rows="2"></textarea>             
        </div>
        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile">
        </div>
        <div class="checkbox">
</div>
        <button type="submit" class="btn btn-default">Submit</button>
</div>
@endsection



















