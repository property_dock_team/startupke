@extends('layouts.appMain')
@section('content')

    <body>
        <div class="container">
       
            <!-- Jumbotron -->
            <div class="jumbotron" style="background-image: url('Images/entrepreneur/entrepreneur1.jpg');  background-size:100% 100%;height: 300px;">
                <h1>Get your start up funded today<br></h1>
                <br>
                <p class="lead"><a class="btn btn-lg btn-success" href="investmentmodel" role="button">Get started today</a><br></p>
            </div>
            <div>
            <H4>Entrepreneur</H4>
            
            <h2>Our Main areas of Investment</h2>
            <br>
            <div class="row marketing">
                <div class="col-lg-4">
                <img class="img-circle" src="Images/entrepreneur/InternetTech.jpg" alt="Generic placeholder image" width="190" height="190">
                        
                    <h3>Internet Technology</h3>
                    <p>In the age of the internet, social connectivity and business networking was completely transformed and made more efficient.This created a huge market  online which intern ment huge opportunity for businesses to increase their revenues through exploiting this online platforn.A great deal of e-commerce bussiness are being established and growing at an incredible rate which is a fact not lost to us.A greate number of new businesses can be found online, and with a wide range of social media platforms available virtually for free, this presents an increduble opportunity for entreprenuers to exloit.  Startup is at the forefront of helping you the entreprenuer realize your growth.</p>
                   </div>
                <div class="col-lg-4">

                <img class="img-circle" src="Images/entrepreneur/SoftwareDevelopment.png" alt="Generic placeholder image" width="190" height="190">
                
                <h3>Software  Development</h3>
                    <p>Technology has become ascimilates into almost every process of our lives and is hard to ignore its importance.Business is especially benefiting from the efficiency and automation that  software systems  provides.Software development is increasingly becomming the most valuable  profession in the business environment today.This is because of the most business processes are being automated through software development.This means that there is a great deal of software businesses are coming up and, we, here at startup capital are very excited to assist you  through the process of growing your business  in an incredibly competitive business environment.</p>
                    
                </div>
                <div class="col-lg-4">
                <img class="img-circle" src="Images/entrepreneur/Agribussiness.jpg" alt="Generic placeholder image" width="190" height="190">
                
                
                <h3>Agribusiness</h3>
                    <p>Agriculture plays a core role in any economy around the world and is one of a very few industries that will never fall short of demand for the products sold. This provides an incredible opportunity for aspirering entreprenuers looking to get into business.Due to the very natural demand  for Agricultural products, we make a very conscious effort to support entreprenuers looking to grow their businesses.By encouraging a more profesional and informed approach, we are optimistic  of agribusiness presenting a huge growth opportunity for many entreprenuers.</p>
                 
                </div>
            </div>

              <h3>Our Mission</h3>
            <br>
            <P>Startup capital is an investment platform that helps startups and small businesses raise capital online. We currently raise capital from  institutional investors and public Crowdfunding. Once you apply we will discuss what option may be right for you.</P>
                    
            </div>
          <p>At startup capital,we are devoted to seeing that we promote bussiness growth. We recorgnise the abadance of new ideas and innovations on pre-existing technologies that entrepreneures are pionearing.This  has informed our desire to make the entrepreneuring process much more effective by gathering all the stake holders in one platform where every one benefits and adds value to one another.

Partnerships are crutial in building a successfull bussiness, so we endevor to help entrepreneures get the right networks and partners in their endevors.</p> <p>The greatest concern to every entrepreneure is access to capital and the right bussiness networks, we, here at startup capital have a partnered with  investors and  consultants from various fields who can help you the entrepreneure start off or grow your bussiness.

We have a particular interest in the technology space where we seek to leverage new technologies in the bussiness world.

Kenya is in abadande of small and medium bussinesses that are looking to grow and we are more than willing to walk with you in your journey.</p>
            
        </div>         
        <!-- /container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
@endsection
