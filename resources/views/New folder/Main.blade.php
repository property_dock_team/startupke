@extends('layouts.appMain')
@section('content')

   
    <body >
        <div class="row">
           
            <!-- Jumbotron -->
            <div class="row" style="background-color: olivedrab">
                <div class="jumbotron"  style="background-image: url('Images/Startup/Startup.jpg');  background-size:100% 100%;height: 300px;">
                
                    <br>
                </div>
            </div>
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-lg-12" style=" background-color: lightgoldenrodyellow">
                        <div class="container">
                        <div class="col-lg-4">
                            <h2>Investors</h2>
                                <p>Our online platform offers investors a gateway to investment opportunities in pre-vetted early stage startups and also secondary investment opportunities. Traditionally these investments have required a larger capital commitment. Not anymore. Now you can access private investment opportunities, sometimes for as little as ksh.5000.</p>
                            <p><a class="btn btn-primary" href="Investor" role="button">View details &raquo;</a></p>
                        </div>
                        <div class="col-lg-4">
                            <h2>Entrepreneur</h2>
                            <p>Startup kenya is an online investment platform that helps startups and small businesses raise capital. We currently raise capital through private and institutional investment and Crowdfunding . Once you apply we will discuss what option may be right for you.</p>
                            <p><a class="btn btn-primary" href="Entrepreneur" role="button">View details &raquo;</a></p>
                        </div>
                        <div class="col-lg-4">
                            <h2>Crowd Funding</h2>
                            <p>Start up kenya offers an equity crowdfunding investment platform.We combining the best of venture capital with equity crowdfunding.  Our team of investor relations associates is committed to personal relationships with our investors to provide high quality customer service to help you to invest in startups.</p>
                            <p><a class="btn btn-primary" href="CrowdFunding" role="button">View details &raquo;</a></p>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>         
        <!-- /container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
@endsection
