@extends('layouts.appMain')
@section('content')

    <body>
        <div class="row">
            
            <!-- Jumbotron -->
            
            <div class="jumbotron"  style="background-image: url('Images/Investor/venture-capital.jpg');  background-size:100% 100%;height: 300px;">
                <h1>Start investing in a start up today<br></h1>
                <br>
                <p class="lead"><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a><br></p>
            </div>
            <div class="row marketing">
                    <div class="container">
                        <div class="col-lg-4">
                            <h3>Institutional Investor</h3>
                            <img class="img-circle" src="Images/Investor/Institutional-Investor.jpg" alt="Generic placeholder image" width="190" height="190">
                        
                            <p>Institutional investors are the biggest component of the so-called "smart money" group. There are generally six types of institutional investors: pension funds, endowment funds, insurance companies, commercial banks, mutual funds and hedge funds.</p>
                        </div>
                        <div class="col-lg-4">
                        <h3>Private investor</h3>
                        <img class="img-circle" src="Images/Investor/private-investor.jpg" alt="Generic placeholder image" width="190" height="190">
                        
                            <p>Private investors are key for new businesses looking to raise start up capital. Not only do private investments bring financial help to the entrepreneur, but often these investors can provide expertise and contacts that the new business may need in order to get to the next level.</p>
                            
                        </div>
                        <div class="col-lg-4">
                        <h3>CrowdFunding</h3>

                        <img class="img-circle" src="Images/Investor/crowdfunding.jpg" alt="Generic placeholder image" width="190" height="190">
                        
                            <p>Crowdfunding makes use of the easy accessibility of vast networks of people through social media and crowdfunding websites to bring investors and entrepreneurs together, and has the potential to increase entrepreneurship by expanding the pool of investors from whom funds can be raised beyond the traditional circle of owners, relatives and venture capitalists.</p>
                        
                        </div>
                    </div>

            </div>

            <div class="row" style=" background-color: aqua">
                <div class="container">
            <h3>Our Mission</h3>
            <br>
            <p>Every investor knows that the most scarse rescourse in bussiness  are good ideas.We at Startup  recorgnise that investors  are looking to find ventures that are innovative and  profitable that they can invest in. This is why we make the conscious effort of partnering  with entrepreneures who are eager to grow their bussiness and need you, the investor to walk  and grow with them.

We recorgnise that there different industries that an investor could have interest in, from agriculture, technology, hotel and restaurants, construction, transport and many more, therefor  we have partnered with a diverse group of entrepreneures who  share the same interests with you and  will help you build profitable portfolio.

Investors may organise themselves in different ways, whether you are an institutional, private or group investor,your interests will be channelled properly, through our  platform that helps you  connect with the right entrepreneure, so as to build a portfolio that is profitable.</p>
          </div>
        </div>  
          </div> 
        <!-- /container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
@endsection
