<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{  asset('css/assets/images/img-20180813-wa0009-122x122.jpg')}}" type="image/x-icon">
  <meta name="description" content="Web Site Maker Description">
  <title>Business Plan</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WJbhG2ku" once="menu" id="menu2-13">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="">responsive web templates</a></section><section class="mbr-section content5 cid-r70aiMPyVC mbr-parallax-background" id="content5-1g">

    

    

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    <br>Startup Systems</h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">
                    Home of Innovation and excellence</h3>
                
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r7062V2DAF" id="content1-1d">
    
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7"><p><strong>About Startup Systems</strong><br></p><p>Startup Systems opened for business in June 2013 and has offices in Nairobi Kenya. Our aim is to address specific needs related to custom application software. A large demand exists for custom developed software at reasonable rates. Startup Kenya develops custom software that is primarily web and mobile based.</p></div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r70674xIXG" id="content2-1e">

     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7">
                <blockquote><strong>We Aid businesses in making their opperatons clear and simple. Every company should use a succinct system to guide its operations.</strong></blockquote>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r70786CdZU" id="content1-1f">
    
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7">
<div>
<span style="font-size: 1rem;">We know that it is more cost effective over the long run to develop bespoke software that is an exact fit for your business. In addition to custom development, we do integrations into a number of third-party services and APIs via Rest, JSON and Soap. This means that your new software will seamlessly work with other systems and share information, all while reducing the cost of ownership and development.

The company has moved into the cloud environment, collecting talented  IT professionals along the way. The growth has culminated in a software development company with a team of project managers, technical developers, mobile developers, application specialists and support technicians. Startup  Systems can assist with any development such as software, web design, mobile apps and is a full solution business.</span></div></div>
        </div>
    </div>
</section>

<section class="mbr-section article content12 cid-r705F8qZ9A" id="content12-1c">


    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text counter-container col-12 col-md-8 mbr-fonts-style display-7">
            <p><strong>Our Products and Services</strong><br></p>
                <ul>
                    <li><strong>Gaming Software Solutions</strong></li>
                    <li><strong>Microfinance Savings and Lending Solution</strong></li>
                    <li><strong>Call Center Solution (Customer Care) </strong></li><li><strong>Web Design & Development</strong> </li><li><strong>Social Media Marketing</strong> </li><li><strong>Search Engine Optimization (SEO)</strong> </li><li><strong>Mobile App Development</strong></li>
                </ul>
            </div>
        </div>
    </div>
</section>



<script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
<script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
<script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
<script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
<script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
<script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
<script src="{{ asset('css/assets/vimeoplayer/jquery.mb.vimeo_player.js')}}"></script>
<script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection