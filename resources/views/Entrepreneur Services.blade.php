<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/img-20180813-wa0009-122x122.jpg" type="image/x-icon">
  <meta name="description" content="Web Builder Description">
  <title>Entreprenuer</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">

  <!-- <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css"> -->
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WK2qUWWD" once="menu" id="menu2-12">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="">free website creation software</a></section><section class="header12 cid-r6VKS2kMJw mbr-parallax-background" id="header12-6">

    

    

    <div class="container  ">
            <div class="media-container">
                <div class="col-md-12 align-center">
                    <h1 class="mbr-section-title pb-3 mbr-white mbr-bold mbr-fonts-style display-1">Get your start up funded today</h1>
                    
                    

                    <div class="icons-media-container mbr-white">
                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                            
                                <span class="mbr-iconfont mbri-globe-2" style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span>
                            
                            </div>
                            <h5 class="mbr-fonts-style display-5">Internet Tech</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                
                                    <span class="mbr-iconfont mbri-sun" style="color: rgb(228, 210, 149); fill: rgb(228, 210, 149);"></span>
                                
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                Creative Idea
                            </h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                
                                    <span class="mbr-iconfont mbri-mobile2" style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span>
                                
                            </div>
                            <h5 class="mbr-fonts-style display-5">Mobile apps</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                
                                    <span class="mbr-iconfont mbri-responsive" style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span>
                                
                            </div>
                            <h5 class="mbr-fonts-style display-5">Tech Hardware</h5>
                        </div>
                    </div>

                    <div class="mbr-section-btn align-center">
                            <a class="btn btn-md btn-primary display-4" href="#content12-1c">
                                <span class="mbr-iconfont mbri-file"></span>Apply Now</a>
                            <a class="btn btn-md btn-white-outline display-4" href="businessplan">
                                <span class="mbr-iconfont mbri-idea"></span>Bisiness Plan</a>
                                <a class="btn btn-md btn-white-outline display-4" href="investmentcriteria">
                                        <span class="mbr-iconfont mbri-idea"></span>Funding Criteria</a>
                        </div>
                </div>
            </div>
    </div>

    
</section>

<section class="features3 cid-r6Wf0sWP8Z" id="features3-e">

    

    
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/mbr-676x451.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Internet Technology</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            In the age of the internet, social connectivity and business networking was completely transformed and made more efficient.This created a huge market online which intern ment huge opportunity for businesses to increase their revenues through exploiting this online platforn.A great deal of e-commerce bussiness are being established and growing at an incredible rate which is a fact not lost to us.A greate number of new businesses can be found online, and with a wide range of social media platforms available virtually for free, this presents an increduble opportunity for entreprenuers to exloit. Startup is at the forefront of helping you the entreprenuer realize your growth.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/mbr-676x380.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Software Development</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Technology has become ascimilates into almost every process of our lives and is hard to ignore its importance.Business is especially benefiting from the efficiency and automation that software systems provides.Software development is increasingly becomming the most valuable profession in the business environment today.This is because of the most business processes are being automated through software development.This means that there is a great deal of software businesses are coming up and, we, here at startup capital are very excited to assist you through the process of growing your business in an incredibly competitive business environment.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/mbr-676x495.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">
                            Agribusiness
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Agriculture plays a core role in any economy around the world and is one of a very few industries that will never fall short of demand for the products sold. This provides an incredible opportunity for aspirering entreprenuers looking to get into business.Due to the very natural demand for Agricultural products, we make a very conscious effort to support entreprenuers looking to grow their businesses.By encouraging a more profesional and informed approach, we are optimistic of agribusiness presenting a huge growth opportunity for many entreprenuers.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            
        </div>
    </div>
</section>









  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
  <script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/vimeoplayer/jquery.mb.vimeo_player.js')}}"></script>
  <script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection