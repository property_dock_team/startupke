
<head>

  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{asset('css/assets/images/img-20180813-wa0009-122x122.jpg')}}" type="image/x-icon">
  <meta name="description" content="">
  <title>Home</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WJbhG2ku" once="menu" id="menu2-z">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="{{asset('css/assets/images/logo2.png')}}" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="https://mobirise.info/r"></a></section><section class="carousel slide cid-r6WiwTypB4" data-interval="false" id="slider1-j">

    

    <div class="full-screen"><div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="4000"><ol class="carousel-indicators"><li data-app-prevent-settings="" data-target="#slider1-j" class=" active" data-slide-to="0"></li><li data-app-prevent-settings="" data-target="#slider1-j" data-slide-to="1"></li><li data-app-prevent-settings="" data-target="#slider1-j" data-slide-to="2"></li></ol><div class="carousel-inner" role="listbox"><div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url({{ asset('css/assets/images/mbr-1920x1152.jpg')}});"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay"></div><img src="{{ asset('css/assets/images/mbr-1920x1152.jpg')}}"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"><h2 class="mbr-fonts-style display-1">Get Funded Today</h2><p class="lead mbr-text mbr-fonts-style display-5">Channel Your Entreprenural spirit</p><div class="mbr-section-btn" buttons="0"><a class="btn btn-success display-4" href="Investor">Investor</a> <a class="btn  btn-white-outline display-4" href="Entrepreneur">Entrepreneur</a></div></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url({{ asset('css/assets/images/mbr-1-1920x1271.jpg')}});"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay" style="opacity: 0.7;"></div><img src="{{ asset('css/assets/images/mbr-1-1920x1271.jpg')}}"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"><h2 class="mbr-fonts-style display-1">Find Startups to invest in</h2><p class="lead mbr-text mbr-fonts-style display-5">Build your portfolio</p><div class="mbr-section-btn" buttons="0"><a class="btn btn-success display-4" href="Investor">Investor</a> <a class="btn  btn-white-outline display-4" href="Entrepreneur">Entrepreneur</a></div></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url({{ asset('css/assets/images/mbr-1920x967.jpg')}});"><div class="container container-slide"><div class="image_wrapper"><div class="mbr-overlay" style="opacity: 0.7;"></div><img src="{{ asset('css/assets/images/mbr-1920x967.jpg')}}"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"><h2 class="mbr-fonts-style display-1">Crowdfunding</h2><p class="lead mbr-text mbr-fonts-style display-5">Start your campaign</p><div class="mbr-section-btn" buttons="0"><a class="btn btn-success display-4" href="Investor">Investor</a> <a class="btn  btn-white-outline display-4" href="Entrepreneur">Entrepreneur</a></div></div></div></div></div></div></div><a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-j"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a><a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-j"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a></div></div>

</section>

<section class="features7 cid-r6W6S8vCi0" id="features7-9">
    
    

    
    <div class="container">
        <div class="row justify-content-center">

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-cash"></span>
                    </div>
                    <div class="card-box media-body">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            Our Products
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           Our online platform offers investors a gateway to investment opportunities in pre-vetted early stage startups and also secondary investment opportunities. Traditionally these investments have required a larger capital commitment. Not anymore. Now you can access private investment opportunities, sometimes for as little as ksh.5000.
                        </p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media"> 
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-growing-chart"></span>
                    </div>
                    <div class="card-box media-body">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            Our Services
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           Startup kenya is an online investment platform that helps startups and small businesses raise capital. We currently raise capital through private and institutional investment and Crowdfunding . Once you apply we will discuss what option may be right for you.
                        </p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media">
                    <div class="card-img pr-2">
                        <span class="mbri-responsive mbr-iconfont"></span>
                    </div>
                    <div class="media-body card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Crowd Funding</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           Start up kenya offers an equity crowdfunding investment platform.We combining the best of venture capital with equity crowdfunding. Our team of investor relations associates is committed to personal relationships with our investors to provide high quality customer service to help you to invest in startups.
                        </p>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</section>




  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/ytplayer/jquery.mb.ytplayer.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js')}}"></script>
  <script src="{{ asset('assets/vimeoplayer/jquery.mb.vimeo_player.js')}}"></script>
  <script src="{{ asset('assets/theme/js/script.js')}}"></script>
  <script src="{{ asset('assets/slidervideo/script.js')}}"></script>
  
  
</body>
@endsection