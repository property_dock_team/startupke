<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/img-20180813-wa0009-122x122.jpg" type="image/x-icon">
  <meta name="description" content="Website Creator Description">
  <title>Investor</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">

  {{--  <!-- <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css"> -->  --}}
  
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
</head>
<body>
  {{--  <section class="menu cid-r6Wid0Ifmg" once="menu" id="menu2-h">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')

<section class="engine"><a href="https://mobirise.info/q">free responsive web templates</a></section><section class="header1 cid-r6WmJWrWPi mbr-parallax-background" id="header1-k">

    

    <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(0, 0, 0);">
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <!-- <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1">Start investing in a start up today</h1>
                 -->
                
                <div class="mbr-section-btn align-center">
                    <a class="btn btn-md btn-primary display-4" href="#features3-l">
                        <span class="mbr-iconfont mbri-file"></span>LEARN MORE</a>
                    <a class="btn btn-md btn-white-outline display-4" href="#">
                        <span class="mbr-iconfont mbri-idea"></span>GET STARTED</a>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="features3 cid-r6WnAftFQb" id="features3-l">

    

    
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/Gamming.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Gaming Software Solutions</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                        Startup Systems is driven by technological innovation and a deep understanding of the gaming industry
and our clients’ business needs. We provide excellent service, quality and reliability that make us the
trusted partner.We have extended connection to many service providers in the industry and have built highly integrated solutions that ensure seamless working of any gaming solution.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/Callcentre.jpeg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Call Center Solution (Customer Care)</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                        Though not specifically a Betting Product, the Call Center solution is included as a package under
Betting to our clients to allow them to provide support to customers / players. The
The Inbound/Outbound focused Customer contact center solution provides;
➔ Automatic Call Distribution (ACD)
➔ Multi-level IVR
➔ Customized Call Queues (Sales, Support, Accounts)
➔ Call Recording
➔ Skills Based Routing
➔ Call Flow Control
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/AutomatedMicrocreditSolution.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">
                            Microfinance Savings and Lending Solution
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                       Our Microfine system is a robust core Sacco system that is delivered via the cloud or hosted privately on premise. It provides the functionality and scalability to scale horizontally across any form of financial inclusion and vertically for an organization of any size. Microfine provides a suite of web and mobile implementations that you can configure and extend to meet your needs.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="#" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            
        </div>
    </div>
</section>
<section class="features3 cid-r6WnAftFQb" id="features3-l">

    

    
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/school-app.png')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">College/School Management Solution</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                       Our school management software is an extensive all-in-one cloud-based, school administration & student information system. Our platform is designed to streamline your school operation, communicate with your entire community and be simple to use while offering extensive configuration. Features include, Registration and admissions, grading, student attendance, communication, bus routes, event management, finance, scheduling and timetabling, learning management - LMS and VLE, parent & student portals and much more.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/pos.png')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Shop/Bar Management System (PoS)</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                        Our Platform brings the restaurant industry a cutting-edge POS system that does more than just payment processing. It gives owners and business leaders the ability to connect with their customers on a different level, by streamlining a customer interaction combined with how every team member responds to orders. The feature-packed platform automates reports, online ordering, and onsite checkouts to get the most of every interaction. With highly flexible packages, small to large businesses can leverage the power of our platform.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/hospital.png')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">
                        Hospital/Dispensary Management Solution
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                        Our platform is a powerful hospital management software developed in YII framework. Having an integrated management software has by now become essential for every potential healthcare institution. The platform is  an effective software specifically designed to fulfill various requirements in managing hospitals. This dedicated software developed by experts in the healthcare field with a comprehensive HR management solutions including patient records, pharmacy, and medical staffs profiles.
                         </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="#" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            
        </div>
    </div>
</section>





  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection